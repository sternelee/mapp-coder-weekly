import { Component } from 'react'
import { Provider } from 'mobx-react'

import counterStore from './store/counter'
import weeklyStore from './store/weekly'

import './app.styl'

const store = {
  counterStore,
  weeklyStore
}

class App extends Component {
  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  // this.props.children 就是要渲染的页面
  render () {
    return (
      <Provider store={store}>
        {this.props.children}
      </Provider>
    )
  }
}

export default App
