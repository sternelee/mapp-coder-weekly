import React from 'react';
import { View } from '@tarojs/components';

interface Props {
  name: string
  size?: number
  color?: string
}

const Icon = (props: Props) => {
  const { name, size = 18, color = "#fff" } = props
  return <View className={`iconfont icon-${name}`} style={{fontSize: size, color }}></View>
}

export default Icon
