import Taro from '@tarojs/taro'
import { Text, Image } from '@tarojs/components'
import { NodeType } from './Node'

interface Props {
  onClick: (href: string, text: string | undefined) => void
  attrs: {
    [key: string]: string
  }
  nodes: NodeType[]
}
function RenderViewLink (props: Props) {
  const { onClick, attrs, nodes } = props
  if (!nodes || nodes.length < 1) return null
  let node: any = nodes[0]
  if (!attrs.href) return null
  if (node.tag === 'em') {
    node = node.nodes[0]
  }
  return (
    node.tag === 'text' ? <Text className={attrs.class || 'link'} style={attrs.style} onClick={() => onClick(attrs.href, node.text)}>{node.text}</Text> : <Image className={node.attrs.class || 'img'} src={node.attrs.src} mode='widthFix' />
  )
}

RenderViewLink.options = {
  addGlobalClass: true
}

export default RenderViewLink
