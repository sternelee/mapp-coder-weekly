// import { useGlobalIconFont } from './components/iconfont/helper';

export default {
  // usingComponents: Object.assign(useGlobalIconFont()),
  pages: [
    'pages/index/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
}
